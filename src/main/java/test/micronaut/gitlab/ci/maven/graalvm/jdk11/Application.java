package test.micronaut.gitlab.ci.maven.graalvm.jdk11;

import io.micronaut.runtime.Micronaut;

public class Application {
    public static void main(String[] args) {
        Micronaut.run(Application.class, args);
    }
}
